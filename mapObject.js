var _ = require('underscore');

const mapObject=(testObject)=>{
    return _.mapObject(testObject, function(val) {
        if(typeof val === 'number'){
            return val*10;
        }
    return val;
  });
}

module.exports={mapObject};